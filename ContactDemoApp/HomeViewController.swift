//
//  HomeViewController.swift
//  ContactDemoApp
//
//  Created by Anjali on 10/06/20.
//  Copyright © 2020 Anjali. All rights reserved.
//

import UIKit
import Auth0

class HomeViewController: UIViewController {
    @IBOutlet var loginBtn: UIButton!
    @IBOutlet var logOutBtn: UIButton!
    @IBOutlet var contactBtn: UIButton!
    
    let credentialsManager = CredentialsManager(authentication: Auth0.authentication())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "ConTact DeMo"
    }
    
    // MARK: Button Action
    @IBAction func showContactAction(_ sender: UIButton) {
        var contactApi = ContactRestApiCalls()
        contactApi.delegate = self
        contactApi.contactApi()
    }
    
    @IBAction func loginAction(_ sender: UIButton) {
        self.loadLoginScreen()
    }
    
    @IBAction func logoutAction(_ sender: UIButton) {
        Auth0
            .webAuth()
            .clearSession(federated:false) {
                switch $0 {
                case true: break
                    
                case false: break
                    
                }
        }
    }
    
    // MARK: Navigation Method
    func performNavigation(contect: NSArray) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let contentController = storyBoard.instantiateViewController(withIdentifier: "ContactTableViewController") as! ContactTableViewController
        contentController.title = title
        contentController.contactArry = contect
        navigationController?.pushViewController(contentController, animated: true)
    }
    
    // MARK: Autho Login Method
    func loadLoginScreen() {
        Auth0
            .webAuth()
            .scope("openid profile")
            .audience("https://dev--8-iw9du.au.auth0.com/userinfo")
            .start {
                switch $0 {
                case .failure(let error):
                    print("Error: \(error)")
                case .success(let credentials):
                    self.loginBtn.isHidden = true
                    print("Credentials: \(credentials)")
                }
        }
    }
}

// MARK: - API delegates'
extension HomeViewController : ContactRestApiCallsDelegate {
    func contactDetailsReceivedSuccessFullly(data: [Dictionary<String, String>]) {
        do {
            print("Success data: \(data)")
            let finalArray = data
            performNavigation(contect: finalArray as NSArray)
        }
    }
    
    func failedToCommunicateWithDevice(error: Error) {
        print("Failure")
    }
}

