//
//  ContactTableViewController.swift
//  ContactDemoApp
//
//  Created by Anjali on 11/06/20.
//  Copyright © 2020 Anjali. All rights reserved.
//

import UIKit
import Contacts

class ContactTableViewController: UITableViewController {
    
    let cellId = "cellId"
    var contactArry = NSArray()
    var name : String = ""
    var number: String = ""
    var profileImage: String = ""
    var twoDimensionalArray = [ExpandableNames]()
    
     // MARK: Conact Api Method
    private func fetchContacts() {
        print("Attempting to fetch contacts today..")
        
        let store = CNContactStore()
        
        store.requestAccess(for: .contacts) { (granted, err) in
            if let err = err {
                print("Failed to request access:", err)
                return
            }
            
            if granted {
                print("Access granted")
                
                let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey]
                let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
                
                do {
                    
                    var favoritableContacts = [FavoritableContact]()
                    
                    try store.enumerateContacts(with: request, usingBlock: { (contact, stopPointerIfYouWantToStopEnumerating) in
                        
                        print(contact.givenName)
                        print(contact.familyName)
                        favoritableContacts.append(FavoritableContact(contact: contact, hasFavorited: false))
                    })
                    
                    let names = ExpandableNames(isExpanded: true, names: favoritableContacts)
                    self.twoDimensionalArray = [names]
                    
                    
                } catch let err {
                    print("Failed to enumerate contacts:", err)
                }
                
            } else {
                print("Access denied..")
            }
        }
        self.tableView.reloadData()
    }
    
    // MARK: View Load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchContacts()
        self.title = "Contacts"
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        tableView.register(ContactCell.self, forCellReuseIdentifier: cellId)
    }
    
    // MARK: Navigation Method
    
    func performNavigation() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let contentController = storyBoard.instantiateViewController(withIdentifier: "DetailContactViewController") as! DetailContactViewController
        contentController.title = title
        contentController.name = name
        contentController.num = number
        contentController.img = profileImage
        navigationController?.pushViewController(contentController, animated: true)
    }
    
     // MARK: TableView Delegates
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let button = UIButton(type: .system)
        button.setTitle("Show Local Numbers", for: .normal)
        if section > 0 {
            button.setTitle("Show Server Numbers", for: .normal)
        }
        button.setTitleColor(.black, for: .normal)
        button.backgroundColor = .yellow
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.tag = section
        
        return button
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 36
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if twoDimensionalArray.count > 0 {
                return twoDimensionalArray[0].names.count
            }
            else
            {
                return 0
            }
        }
        else {
            return contactArry.count
        }
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ContactCell(style: .subtitle, reuseIdentifier: cellId)
        if indexPath.section == 0 {
            let favoritableContact = twoDimensionalArray[0].names[indexPath.row]
            cell.textLabel?.text = favoritableContact.contact.givenName + " " + favoritableContact.contact.familyName
            cell.detailTextLabel?.text = favoritableContact.contact.phoneNumbers.first?.value.stringValue
            
        } else {
            let contentDic = contactArry[indexPath.row] as! Dictionary<String, String>
            cell.textLabel?.text = (contentDic["firstname"] ?? "") + " " + (contentDic["lastname"] ?? "")
            cell.detailTextLabel?.text = contentDic["number"]
            print("dic:\(contentDic["firstname"] ?? "")")
        }
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        name = ""; profileImage = ""; number = ""
        if indexPath.section == 0 {
            let favoritableContact = twoDimensionalArray[0].names[indexPath.row]
            name = favoritableContact.contact.givenName + " " + favoritableContact.contact.familyName
            number = (favoritableContact.contact.phoneNumbers.first?.value.stringValue)!
            
        } else {
            let contentDic = contactArry[indexPath.row] as! Dictionary<String, String>
            name = (contentDic["firstname"] ?? "") + " " + (contentDic["lastname"] ?? "")
            number = contentDic["number"] ?? ""
            profileImage = contentDic["profileImg"] ?? ""
            
        }
        self.performNavigation()
    }
    
}

