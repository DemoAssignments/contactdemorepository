//
//  DetailContactViewController.swift
//  ContactDemoApp
//
//  Created by Anjali on 12/06/20.
//  Copyright © 2020 Anjali. All rights reserved.
//

import UIKit

class DetailContactViewController: UIViewController {
    
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var phoneNumLabel: UILabel!
    var name: String = ""
    var num: String = ""
    var img: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = ""
        if img == "" {
            img = "http://cdn.playbuzz.com/cdn/38402fff-32a3-4e78-a532-41f3a54d04b9/cc513a85-8765-48a5-8481-98740cc6ccdc.jpg"
        }
        let url = URL(string: img)
        self.downloadImage(from: url!)
        self.setupData()
        
    }
    
    func setupData() {
        nameLabel.text = name
        phoneNumLabel.text = num
    }
    
    // MARK: ImageSetup
    func downloadImage(from url: URL) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            print("data")
            guard let data = data, let image = UIImage(data: data) else { return }
            DispatchQueue.main.async {
                self.profileImage.image = image
            }
        }.resume()
      
    }
}
