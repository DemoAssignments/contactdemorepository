//
//  ExpandableNames.swift
//  ContactDemoApp
//
//  Created by Anjali on 11/06/20.
//  Copyright © 2020 Anjali. All rights reserved.
//

import Foundation
import Contacts

struct ExpandableNames {
    var isExpanded: Bool
    var names: [FavoritableContact]
}

struct FavoritableContact {
    let contact: CNContact
    var hasFavorited: Bool
}
