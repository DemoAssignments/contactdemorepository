//
//  ContactApiCall.swift
//  ContactDemoApp
//
//  Created by Anjali on 12/06/20.
//  Copyright © 2020 Anjali. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

protocol ContactRestApiCallsDelegate {
    func contactDetailsReceivedSuccessFullly(data: [Dictionary<String, String>])
    func failedToCommunicateWithDevice(error: Error)
}
struct ContactRestApiCalls {
    
    var delegate : ContactRestApiCallsDelegate?
    
    
    func contactApi() {
        
        AF.request(contactDemoURLs.contactDemoBaseURL, method: .get, headers:nil).response {
            (response) in
            switch response.result {
            case .success( _):
                print("** SUCCESS Details ***")
                do {
                    guard let result = response.data else {
                        print("Error")
                        return
                    }
                    let stringJson = String.init(data: result, encoding: .isoLatin1)
                    guard let data = stringJson?.data(using: .utf8) else { return }
                    let deviceResponse : JSON = try JSON.init(data: data, options: .fragmentsAllowed)
                    let dataN = deviceResponse["data"]
                    var contentArray: [Dictionary<String, String>] = []
                    print(deviceResponse)
                    print("*************")
                    print(dataN)
                    let arrayNew = dataN
                    for (key, value) in arrayNew {
                        print("new: \(key)")
                        print("new: \(value)")
                        var contentDic = [String: String]()
                        contentDic["firstname"] = value["first_name"].rawValue as? String
                        contentDic["lastname"] = value["last_name"].rawValue as? String
                        let num = value["mobile_number"].rawValue as? Int
                        contentDic["number"] = "\(num ?? 0)"
                        contentDic["profileImg"] = value["profile_image"].rawValue as? String
                        contentArray.append(contentDic)
                    }
                     print("FinalArray:\(contentArray)")
                    self.delegate?.contactDetailsReceivedSuccessFullly(data: contentArray)
                    
                } catch {
                    print(error)
                }
            case .failure(_):
                print("Error loging in: \(String(describing: response.error))")
            }
        }
    }
}
